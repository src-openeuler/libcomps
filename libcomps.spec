Name:           libcomps
Version:        0.1.21
Release:        2
Summary:        Comps XML file manipulation library
License:        GPL-2.0-or-later
URL:            https://github.com/rpm-software-management/libcomps
Source0:        %{url}/archive/refs/tags/%{version}.tar.gz

BuildRequires:  gcc gcc-c++ cmake zlib-devel libxml2-devel check-devel expat-devel

%description
Libcomps is library for structure-like manipulation with content of
comps XML files. Supports read/write XML file, structure(s) modification.

%package        devel
Summary:        Development files for libcomps library
Requires:       %{name} = %{version}-%{release}
%description    devel
Development files for libcomps library.

%package        help
Summary:        Documentation files for libcomps library
Requires:       %{name} = %{version}-%{release}
BuildArch:      noarch
BuildRequires:  python3-sphinx
BuildRequires:  doxygen
Provides:       %{name}-doc = %{version}-%{release} python-%{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release} python-%{name}-doc < %{version}-%{release}

%description    help
Documentation files for libcomps library and python bindings libcomps library.

%package -n     python3-%{name}
Summary:        Python 3 bindings for libcomps library
BuildRequires:  python3-devel
%{?python_provide:%python_provide python3-%{name}}
Requires:       %{name} = %{version}-%{release}
Obsoletes:      platform-python-%{name} < %{version}-%{release}
%description -n python3-%{name}
Python3 bindings for libcomps library.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake -S libcomps -DPYTHON_DESIRED:STRING=3
%cmake_build
%cmake_build --target docs
%cmake_build --target pydocs

%install
%cmake_install

mv %{__cmake_builddir}/src/python/docs/html %{__cmake_builddir}/src/python/docs/pydocs-html

%check
pushd %{__cmake_builddir}
make test
make pytest
popd

%files
%license COPYING
%doc README.md
%{_libdir}/%{name}.so.*

%files devel
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/%{name}/

%files help
%doc %{__cmake_builddir}/docs/libcomps-doc/html
%doc %{__cmake_builddir}/src/python/docs/pydocs-html

%files -n python3-%{name}
%{python3_sitearch}/%{name}/
%{python3_sitearch}/%{name}-%{version}-py%{python3_version}.egg-info

%changelog
* Fri Nov 08 2024 Funda Wang <fundawang@yeah.net> - 0.1.21-2
- adopt to new cmake macro

* Mon Jun 17 2024 yangxiaodong <yangxiaodong@kylinos.cn> - 0.1.21-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update version to 0.1.21

* Sat Jan 28 2023 fuanan <fuanan3@h-partners.com> - 0.1.19-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update version to 0.1.19

* Sat Oct 29 2022 dongyuzhen <dongyuzhen@h-partners.com> - 0.1.18-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Rebuild for next release

* Fri Dec 31 2021 wangchen <wangchen137@huawei.com> - 0.1.18-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update to 0.1.18

* Tue Apr 28 2020 zhouyihang <zhouyihang3@huawei.com> - 0.1.15-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update libcomps version to 0.1.15

* Mon Jun 22 2020 chenditang<chenditang1@huawei.com> - 0.1.8-21
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Fix Python method descriptors for Python-3.8.

* Thu Feb 20 2020 chengquan<chengquan3@huawei.com> - 0.1.8-20
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Update python-sphinx support to python3

* Wed Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.1.8-19
- del requires

* Tue Sep 24 2019 shenyangyang<shenyangyang4@huawei.com> - 0.1.8-18
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise help package

* Thu Aug 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.1.8-17
- Type:NA
- ID:NA
- SUG:NA
- DESC:rewrite spec file

* Wed Aug 21 2019 Zhipeng Xie <xiezhipeng1@huawei.com> - 0.1.8-16
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:modify patch name

* Mon Jun 3 2019 gaoyi <gaoyi15@huawei.com> - 0.1.8-15
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:CVE for libcomps
       https://github.com/rpm-software-management/libcomps/commit/e3a5d056633677959ad924a51758876d415e7046

* Fri Jul 13 2018 openEuler Buildteam <buildteam@openeuler.org> - 0.1.8-14
- Package init
